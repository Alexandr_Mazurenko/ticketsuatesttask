package com.alehandro.ticketsuatesttask.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.alehandro.ticketsuatesttask.DI.TestTaskApplication;
import com.alehandro.ticketsuatesttask.Presenter.ProductListAdapterPresenter;
import com.alehandro.ticketsuatesttask.R;
import com.alehandro.ticketsuatesttask.Util.ProductListAnimation;
import com.alehandro.ticketsuatesttask.View.ProductItemViewHolder;

import javax.inject.Inject;

public class ProductListAdapter extends RecyclerView.Adapter<ProductItemViewHolder> {
    @Inject
    ProductListAdapterPresenter mPresenter;

    public ProductListAdapter() {
        TestTaskApplication.getViewComponent().inject(this);
    }

    @Override
    public ProductItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProductItemViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ProductItemViewHolder holder, int position) {
        holder.setProductName(mPresenter.getItemByPosition(position).productName());
        holder.setProductPrice(mPresenter.getItemByPosition(position).productPrice());
        holder.setProductAvailability(mPresenter.getItemByPosition(position).productAvailability());
        ProductListAnimation.animateOnBindView(holder.itemView);
    }

    @Override
    public int getItemCount() {
        return mPresenter.getItemCount();
    }
}
