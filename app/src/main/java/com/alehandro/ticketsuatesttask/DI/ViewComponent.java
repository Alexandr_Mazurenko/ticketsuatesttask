package com.alehandro.ticketsuatesttask.DI;

import com.alehandro.ticketsuatesttask.Adapter.ProductListAdapter;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Alehandro on 29.03.2017.
 */
@Singleton
@Component(modules = ViewModule.class)
public interface ViewComponent {
    void inject(ProductListAdapter productListAdapter);
}
