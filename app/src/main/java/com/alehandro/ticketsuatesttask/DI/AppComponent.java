package com.alehandro.ticketsuatesttask.DI;

import com.alehandro.ticketsuatesttask.Model.Model;
import com.alehandro.ticketsuatesttask.Presenter.MainActivityPresenter;
import com.alehandro.ticketsuatesttask.Presenter.ProductListAdapterPresenter;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Alehandro on 29.03.2017.
 */
@Singleton
@Component(modules = AppModule.class)
public interface  AppComponent {
    void inject (Model model);
    void inject (MainActivityPresenter mainActivityPresenter);
    void inject (ProductListAdapterPresenter productListAdapterPresenter);
}
