package com.alehandro.ticketsuatesttask.DI;

import com.alehandro.ticketsuatesttask.Presenter.ProductListAdapterPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Alehandro on 29.03.2017.
 */
@Module
public class ViewModule {
    @Provides
    @Singleton
    ProductListAdapterPresenter getProductListAdapterPresenter(){
        return new ProductListAdapterPresenter();
    }
}
