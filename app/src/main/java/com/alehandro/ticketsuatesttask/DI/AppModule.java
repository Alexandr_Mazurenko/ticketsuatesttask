package com.alehandro.ticketsuatesttask.DI;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.alehandro.ticketsuatesttask.Constants.Constants;
import com.alehandro.ticketsuatesttask.Model.Model;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Alehandro on 29.03.2017.
 */
@Module
public class AppModule {
    Application mApplication;

    public AppModule(Application application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    Model getModel() {
        return new Model();
    }

    @Provides
    @Singleton
    SharedPreferences getSharedPreferences(){
        return mApplication.getSharedPreferences(Constants.APP_PREFERENCES, Context.MODE_PRIVATE);
    }

}
