package com.alehandro.ticketsuatesttask.Model;

import android.content.SharedPreferences;

import com.alehandro.ticketsuatesttask.Constants.Constants;
import com.alehandro.ticketsuatesttask.DI.TestTaskApplication;
import com.alehandro.ticketsuatesttask.Model.POJO.Product;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

public class Model {
    private List<Product> mProductList;
    private List<Product> mCurrentProductList;
    private boolean mAvailabilityFilter;
    private String mSortMode;
    private Integer mMaxPrice;
    private Integer mMinPrice;
    private Integer mMaxFilteredPrice;
    private Integer mMinFilteredPrice;
    @Inject
    SharedPreferences mSharedPreferences;

    public Model() {
        TestTaskApplication.getAppComponent().inject(this);
        initApplicationModel();
    }

    private void initApplicationModel() {
        mProductList = new ArrayList<>();
        mCurrentProductList = new ArrayList<>();
        productListSetDefaultValues();

        mCurrentProductList.addAll(mProductList);
        setMaxProductPrice();
        setMinProductPrice();
        mMinFilteredPrice = mMinPrice;
        mMaxFilteredPrice = mMaxPrice;

        mSortMode = mSharedPreferences.getString(Constants.KEY_SORT_MODE, Constants.SORT_MODE_BY_NAME);
        mAvailabilityFilter = mSharedPreferences.getBoolean(Constants.KEY_FILTER_AVAILABILITY, false);

        if (mSharedPreferences.getInt(Constants.KEY_MIN_FILTERED_PRICE, 0) > 0) {
            mMinFilteredPrice = mSharedPreferences.getInt(Constants.KEY_MIN_FILTERED_PRICE, 0);
        }

        if (mSharedPreferences.getInt(Constants.KEY_MAX_FILTERED_PRICE, 0) > 0) {
            mMaxFilteredPrice = mSharedPreferences.getInt(Constants.KEY_MAX_FILTERED_PRICE, 0);
        }

    }

    public Integer getMaxFilteredPrice() {
        return mMaxFilteredPrice;
    }

    public void setMaxFilteredPrice(Integer mMaxFilteredPrice) {
        this.mMaxFilteredPrice = mMaxFilteredPrice;
    }

    public Integer getMinFilteredPrice() {
        return mMinFilteredPrice;
    }

    public void setMinFilteredPrice(Integer mMinFilteredPrice) {
        this.mMinFilteredPrice = mMinFilteredPrice;
    }

    private void setMaxProductPrice() {
        mMaxPrice = Collections.max(mProductList, (o1, o2) -> o1.productPrice()
                .compareTo(o2.productPrice())).productPrice();
    }

    private void setMinProductPrice() {
        mMinPrice = Collections.max(mProductList, (o1, o2) -> o2.productPrice()
                .compareTo(o1.productPrice())).productPrice();
    }

    public Integer getMaxPrice() {
        return mMaxPrice;
    }

    public Integer getMinPrice() {
        return mMinPrice;
    }

    public List<Product> getCurrentProductList() {
        return mCurrentProductList;
    }

    public void setCurrentProductList(List<Product> list) {
        mCurrentProductList = list;
    }

    public boolean isAvailabilityFilter() {
        return mAvailabilityFilter;
    }

    public void setAvailabilityFilter(boolean availabilityFilter) {
        this.mAvailabilityFilter = availabilityFilter;
    }

    public void setSortMode(String sortMode) {
        this.mSortMode = sortMode;
    }

    public String getSortMode() {
        return mSortMode;
    }

    public List<Product> getProductList() {
        return mProductList;
    }

    public void saveStateToPrefs() {
        mSharedPreferences.edit()
                .putString(Constants.KEY_SORT_MODE, mSortMode)
                .putBoolean(Constants.KEY_FILTER_AVAILABILITY, mAvailabilityFilter)
                .putInt(Constants.KEY_MIN_FILTERED_PRICE, mMinFilteredPrice)
                .putInt(Constants.KEY_MAX_FILTERED_PRICE, mMaxFilteredPrice)
                .apply();
    }

    private void productListSetDefaultValues() {
        mProductList.add(Product.create("Doogee X9 Mini (White)", 1799, true));
        mProductList.add(Product.create("Doogee X5 Max (White)", 2099, true));
        mProductList.add(Product.create("Huawei Y3 II Black", 2199, false));
        mProductList.add(Product.create("Prestigio Grace Q5 (Blue)", 2399, true));
        mProductList.add(Product.create("Blackview E7s (Stardust Grey)", 2499, true));
        mProductList.add(Product.create("Doogee Shoot 2 (Black)", 2599, false));
        mProductList.add(Product.create("Samsung Galaxy J1 (2016) Black", 2799, false));
        mProductList.add(Product.create("Doogee T5s Black", 2899, true));
        mProductList.add(Product.create("Blackview A9 Pro (Matte Black)", 2999, true));
        mProductList.add(Product.create("Doogee Y6 16Gb (Black)", 3199, true));
        mProductList.add(Product.create("Meizu M5 Matte Black", 3399, false));
        mProductList.add(Product.create("iPhone 7 Plus (Jet Black)", 32999, true));
        mProductList.add(Product.create("Samsung Galaxy S8+ (Orchid Gray)", 28999, true));
        mProductList.add(Product.create("Google Pixel 128Gb", 25999, true));
        mProductList.add(Product.create("Huawei P10 Plus 64Gb", 19999, true));
        mProductList.add(Product.create("Xiaomi Mi Note 2 4/64Gb", 18999, false));
        mProductList.add(Product.create("OnePlus 3T 128Gb ", 15999, true));
    }

}
