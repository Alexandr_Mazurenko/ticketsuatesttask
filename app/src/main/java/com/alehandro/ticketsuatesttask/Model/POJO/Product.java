package com.alehandro.ticketsuatesttask.Model.POJO;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class Product {
    public static Product create(String productName, int productPrice, boolean productAvailability) {
        return new AutoValue_Product(productName, productPrice, productAvailability);
    }

    public abstract String productName();

    public abstract Integer productPrice();

    public abstract boolean productAvailability();
}
