package com.alehandro.ticketsuatesttask.Presenter;

import com.alehandro.ticketsuatesttask.DI.TestTaskApplication;
import com.alehandro.ticketsuatesttask.Interface.IAdapterPresenter;
import com.alehandro.ticketsuatesttask.Model.Model;
import com.alehandro.ticketsuatesttask.Model.POJO.Product;

import javax.inject.Inject;

public class ProductListAdapterPresenter implements IAdapterPresenter{
    @Inject
    Model mModel;

    public ProductListAdapterPresenter(){
        TestTaskApplication.getAppComponent().inject(this);
    }

    @Override
    public int getItemCount() {
        return mModel.getCurrentProductList().size();
    }

    @Override
    public Product getItemByPosition(int position) {
        return mModel.getCurrentProductList().get(position);
    }
}
