package com.alehandro.ticketsuatesttask.Presenter;

import android.util.Log;

import com.alehandro.ticketsuatesttask.Constants.Constants;
import com.alehandro.ticketsuatesttask.DI.TestTaskApplication;
import com.alehandro.ticketsuatesttask.Interface.IView;
import com.alehandro.ticketsuatesttask.Model.Model;
import com.alehandro.ticketsuatesttask.Model.POJO.Product;
import com.alehandro.ticketsuatesttask.Util.ProductListFilter;
import com.alehandro.ticketsuatesttask.Util.ProductListSorter;
import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

@InjectViewState
public class MainActivityPresenter extends MvpPresenter<IView> {
    @Inject
    Model mModel;
    private List<Product> bufferList;

    public MainActivityPresenter() {
        TestTaskApplication.getAppComponent().inject(this);
        bufferList = new ArrayList<>();
        getViewState().initUI();
        ProductListSorter.sortProducts(mModel.getCurrentProductList(), mModel.getSortMode());
        filterProductList();
    }

    public void menuItemSelectHandle(int menuItem) {
        if (menuItem == Constants.MENU_ITEM_SORT) {
            getViewState().showSortDialog();
        } else {
            Log.i(Constants.LOG_TAG, "filter item");
            getViewState().showFilterDialog();
        }
    }

    public void hideDialogs() {
        getViewState().hideFilterDialog();
        getViewState().hideSortDialog();
    }

    public void saveCurrentStateToPrefs() {
        mModel.saveStateToPrefs();
    }

    public Integer getMaxPrice() {
        return mModel.getMaxPrice();
    }

    public Integer getMinPrice() {
        return mModel.getMinPrice();
    }

    public Integer getMinFilteredPrice() {
        return mModel.getMinFilteredPrice();
    }

    public Integer getMaxFilteredPrice() {
        return mModel.getMaxFilteredPrice();
    }

    public void setMinFilteredPrice(Integer price) {
        mModel.setMinFilteredPrice(price);
    }

    public void setMaxFilteredPrice(Integer price) {
        mModel.setMaxFilteredPrice(price);
    }

    public void setAvailabilityFilter(boolean isAvailable) {
        mModel.setAvailabilityFilter(isAvailable);
    }

    public boolean getAvailabilityFilter() {
        return mModel.isAvailabilityFilter();
    }

    public void filterProductList() {
        bufferList = mModel.getProductList();
        if (mModel.isAvailabilityFilter()) {
            bufferList = ProductListFilter.filterByAvailability(bufferList);
        }
        bufferList = ProductListFilter.filterByPrice(bufferList, mModel.getMinFilteredPrice(),
                    mModel.getMaxFilteredPrice());

        mModel.setCurrentProductList(bufferList);
        ProductListSorter.sortProducts(mModel.getCurrentProductList(), mModel.getSortMode());
        getViewState().updateUI();
    }

    public void sortProductListByAction(int sortOption) {
        switch (sortOption) {
            case 0:
                mModel.setSortMode(Constants.SORT_MODE_BY_NAME);
                break;
            case 1:
                mModel.setSortMode(Constants.SORT_MODE_BY_PRICE);
                break;
        }
        ProductListSorter.sortProducts(mModel.getCurrentProductList(), mModel.getSortMode());
        getViewState().updateUI();
    }

}



