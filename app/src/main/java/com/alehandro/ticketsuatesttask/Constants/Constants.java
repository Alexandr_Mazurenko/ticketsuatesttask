package com.alehandro.ticketsuatesttask.Constants;

import com.alehandro.ticketsuatesttask.R;

/**
 * Created by Alehandro on 30.03.2017.
 */

public class Constants {
    public final static String LOG_TAG = "TicketsUATestTask";
    public final static int MENU_ITEM_SORT = R.id.sortItem;
    public final static int MENU_ITEM_FILTER = R.id.filterItem;
    public final static String SORT_MODE_BY_NAME = "byName";
    public final static String SORT_MODE_BY_PRICE = "byPrice";
    public final static String APP_PREFERENCES = "preferences";
    public final static String KEY_SORT_MODE = "sortMode";
    public final static String KEY_FILTER_AVAILABILITY = "filterAvailability";
    public final static String KEY_MIN_FILTERED_PRICE = "minFilteredPrice";
    public final static String KEY_MAX_FILTERED_PRICE = "maxFilteredPrice";
    }
