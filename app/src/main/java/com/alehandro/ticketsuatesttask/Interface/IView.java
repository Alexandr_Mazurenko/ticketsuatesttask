package com.alehandro.ticketsuatesttask.Interface;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * Created by Alehandro on 29.03.2017.
 */

public interface IView extends MvpView{
    void initUI();
    void updateUI();
    @StateStrategyType(AddToEndSingleStrategy.class)
    void showSortDialog();
    void hideSortDialog();
    @StateStrategyType(AddToEndSingleStrategy.class)
    void showFilterDialog();
    void hideFilterDialog();
}
