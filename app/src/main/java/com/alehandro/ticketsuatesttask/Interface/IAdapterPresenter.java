package com.alehandro.ticketsuatesttask.Interface;

import com.alehandro.ticketsuatesttask.Model.POJO.Product;

public interface IAdapterPresenter {
     int getItemCount();
     Product getItemByPosition(int position);
}
