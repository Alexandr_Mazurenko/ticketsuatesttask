package com.alehandro.ticketsuatesttask.Util;

import android.view.View;
import android.view.animation.AlphaAnimation;

public class ProductListAnimation {
    private ProductListAnimation(){}
    public static void animateOnBindView (View view){
        AlphaAnimation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(1000);
        view.startAnimation(animation);
    }
}
