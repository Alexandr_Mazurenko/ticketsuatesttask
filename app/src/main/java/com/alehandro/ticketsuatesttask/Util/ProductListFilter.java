package com.alehandro.ticketsuatesttask.Util;

import com.alehandro.ticketsuatesttask.Model.POJO.Product;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Predicate;


public class ProductListFilter {
    private ProductListFilter(){}

    public  static List<Product> filterByAvailability(List<Product> targetList) {
        List<Product> buffer = new ArrayList<>();
        Observable.fromIterable(targetList)
                .filter(product1 -> product1.productAvailability())
                .subscribe(product -> buffer.add(product));
        return buffer;
    }

    public  static List<Product> filterByPrice(List<Product> targetList, Integer minPrice, Integer maxPrice) {
        List<Product> buffer = new ArrayList<>();
        Observable.fromIterable(targetList)
                .filter(product -> product.productPrice() > minPrice
                        && product.productPrice() < maxPrice)
                .subscribe(product -> buffer.add(product));
        return buffer;
    }
}
