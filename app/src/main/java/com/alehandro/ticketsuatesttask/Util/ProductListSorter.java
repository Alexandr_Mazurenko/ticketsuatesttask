package com.alehandro.ticketsuatesttask.Util;

import com.alehandro.ticketsuatesttask.Constants.Constants;
import com.alehandro.ticketsuatesttask.Model.POJO.Product;

import java.util.Collections;
import java.util.List;

public class ProductListSorter {
    private ProductListSorter(){}

    public static void sortProducts(List<Product> products, String sortMode){
        switch (sortMode){
            case Constants.SORT_MODE_BY_NAME:
                Collections.sort(products, (o1, o2) -> o1.productName().compareTo(o2.productName()));
                break;
            case Constants.SORT_MODE_BY_PRICE:
                Collections.sort(products, (o1, o2) -> o1.productPrice().compareTo(o2.productPrice()));
                break;
        }

    }
}
