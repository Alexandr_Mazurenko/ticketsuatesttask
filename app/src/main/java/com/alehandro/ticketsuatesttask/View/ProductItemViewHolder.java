package com.alehandro.ticketsuatesttask.View;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.alehandro.ticketsuatesttask.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alehandro on 29.03.2017.
 */

public class ProductItemViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.productName)
    TextView mProductName;
    @BindView(R.id.productPrice)
    TextView mProductPrice;
    @BindView(R.id.productAvailability)
    TextView mProductAvailability;
    public ProductItemViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void setProductName(String productName) {
        mProductName.setText(productName);
    }

    public void setProductPrice(int price) {
        mProductPrice.setText(String.valueOf(price));
    }

    public void setProductAvailability(boolean available) {
        if(available){mProductAvailability.setText("Avaliable");
        }else mProductAvailability.setText("Unavaliable");

    }
}
