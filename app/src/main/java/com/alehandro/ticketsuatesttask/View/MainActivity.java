package com.alehandro.ticketsuatesttask.View;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.alehandro.ticketsuatesttask.Adapter.ProductListAdapter;
import com.alehandro.ticketsuatesttask.Interface.IView;
import com.alehandro.ticketsuatesttask.Presenter.MainActivityPresenter;
import com.alehandro.ticketsuatesttask.R;
import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarFinalValueListener;
import com.crystal.crystalrangeseekbar.widgets.BubbleThumbRangeSeekbar;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;

import butterknife.BindArray;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends MvpAppCompatActivity implements IView {
    @InjectPresenter
    MainActivityPresenter mPresenter;
    @BindView(R.id.productList)
    RecyclerView mProductList;
    @BindString(R.string.sort_dialog_title)
    String mSortDialogTitle;
    @BindArray(R.array.sort_options)
    String[] mSortOptions;
    @BindString(R.string.filter_dialog_title)
    String mFilterDialogTitle;
    @BindString(R.string.filter_dialog_positive_button_title)
    String mFilterPositiveButtonTitle;
    @BindString((R.string.filter_dialog_negative_button_title))
    String mFilterNegativeButtonTitle;
    private ProductListAdapter mAdapter;
    private AlertDialog mSortAlertDialog;
    private AlertDialog mFilterAlertDialog;
    private AlertDialog.Builder mAlertBuilder;
    private View mFilterDialogCustomView;
    private int mMaxFilteredPrice;
    private int mMinFilteredPrice;
    private boolean mIsAvailabilityChecked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void initUI() {
        ButterKnife.bind(this);
        mProductList.setHasFixedSize(true);
        mProductList.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new ProductListAdapter();
        mProductList.setAdapter(mAdapter);
    }

    @Override
    public void updateUI() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void showSortDialog() {
        mAlertBuilder = new AlertDialog.Builder(this);
        mAlertBuilder
                .setTitle(mSortDialogTitle)
                .setItems(mSortOptions, (dialog, which) -> {
                    mPresenter.sortProductListByAction(which);
                    mPresenter.hideDialogs();
                });
        mSortAlertDialog = mAlertBuilder.create();
        mSortAlertDialog.show();
    }

    @Override
    public void hideSortDialog() {
        if (mSortAlertDialog != null) mSortAlertDialog.dismiss();
    }

    @Override
    public void showFilterDialog() {
        mMinFilteredPrice=0;
        mMaxFilteredPrice=0;
        mAlertBuilder = new AlertDialog.Builder(this);
        mFilterDialogCustomView = View.inflate(this, R.layout.alert_dialog_filter_custom, null);
        initPriceRangeSeekBar(mFilterDialogCustomView);
        initAvailabilityCheckBox(mFilterDialogCustomView);
        mAlertBuilder
                .setView(mFilterDialogCustomView)
                .setTitle(mFilterDialogTitle)
                .setPositiveButton(mFilterPositiveButtonTitle,
                        (dialog, which) -> {
                            mPresenter.hideDialogs();
                            if(mMaxFilteredPrice>0) {
                                mPresenter.setMaxFilteredPrice(mMaxFilteredPrice);
                            }
                            if(mMinFilteredPrice>0) {
                                mPresenter.setMinFilteredPrice(mMinFilteredPrice);
                            }
                            mPresenter.setAvailabilityFilter(mIsAvailabilityChecked);
                            mPresenter.filterProductList();
                        })
                .setNegativeButton(mFilterNegativeButtonTitle, (dialog, which) ->
                        mPresenter.hideDialogs());
        mFilterAlertDialog = mAlertBuilder.create();
        mFilterAlertDialog.show();
    }

    @Override
    public void hideFilterDialog() {
        if (mFilterAlertDialog != null) mFilterAlertDialog.hide();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        mPresenter.menuItemSelectHandle(item.getItemId());
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        mPresenter.saveCurrentStateToPrefs();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void initPriceRangeSeekBar(View customView) {
        final BubbleThumbRangeSeekbar rangeSeekbar = (BubbleThumbRangeSeekbar) customView
                .findViewById(R.id.priceRangebar);
        rangeSeekbar.setDataType(CrystalRangeSeekbar.DataType.INTEGER);
        rangeSeekbar.setMinValue(mPresenter.getMinPrice()).setMaxValue(mPresenter.getMaxPrice()).apply();
        rangeSeekbar.setMinStartValue(mPresenter.getMinFilteredPrice())
                    .setMaxStartValue(mPresenter.getMaxFilteredPrice()).apply();
        final TextView tvMinPrice = (TextView) mFilterDialogCustomView.findViewById(R.id.tvMinPrice);
        final TextView tvMaxPrice = (TextView) mFilterDialogCustomView.findViewById(R.id.tvMaxPrice);
        rangeSeekbar.setOnRangeSeekbarChangeListener((minValue, maxValue) -> {
            tvMinPrice.setText(String.valueOf(minValue));
            tvMaxPrice.setText(String.valueOf(maxValue));
        });
        rangeSeekbar.setOnRangeSeekbarFinalValueListener(new OnRangeSeekbarFinalValueListener() {
            @Override
            public void finalValue(Number minValue, Number maxValue) {
                if(maxValue.intValue()>0) {
                    mMaxFilteredPrice = maxValue.intValue();
                }
                if(maxValue.intValue()>0) {
                    mMinFilteredPrice = minValue.intValue();
                }

            }
        });
    }

    private void initAvailabilityCheckBox(View customView) {
        final CheckBox availabilityCheckBox = (CheckBox) customView
                .findViewById(R.id.availabilityCheckBox);
        availabilityCheckBox.setChecked(mPresenter.getAvailabilityFilter());
        availabilityCheckBox.setOnCheckedChangeListener((buttonView, isChecked) ->
                mIsAvailabilityChecked = isChecked);

    }


}
